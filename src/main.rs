extern crate reqwest;
extern crate select;

use reqwest::Response;
use std::cell::RefCell;
use select::document::Document;
use select::predicate::Name;
use std::path::Path;


struct Data {
    // Mutable vector for storing links to remote files
    big_files: RefCell<Vec<String>>,
    // Mutable vector for storing pages to extract data from
    html_pages: RefCell<Vec<String>>,
    // Mutable vector for storing links to external pages
    external_pages: RefCell<Vec<String>>,
}

impl Data {
    pub fn created()->Data {
        Data {
            big_files: RefCell::new(Vec::new()),
            html_pages: RefCell::new(Vec::new()),
            external_pages: RefCell::new(Vec::new()),
        }
    }
    pub fn add_bigfile_url(&self, value: String){
        self.big_files.borrow_mut().push(value);
    }
    pub fn add_html_page(&self, value: String){
        self.html_pages.borrow_mut().push(value);
    }
    pub fn add_external_link(&self, value: String){
        self.external_pages.borrow_mut().push(value);
    }
}

fn main() {

    let arguments: Vec<String> = std::env::args().collect();

    if arguments.len() != 2 {
        println!("Usage: indexof_downloader <URL with index of>");
        std::process::exit(0);
    }

    let base_url = &arguments[1];
    let project_name = String::from("project_name");

    if !Path::new(&project_name).exists() {
        if create_project_folder(&project_name) == -1 {
            println!("Could not create folder for this project");
            std::process::exit(1);
        }
    }

    let url_store: Data = Data::created();
    //url_store.add_bigfile_url(String::from("url 1"));


    let mut response = match reqwest::get(base_url.as_str()) {
        Ok(resp) => resp,
        Err(error) => panic!("{}", error)
    };

    // Request was not successfull
    if !response.status().is_success() {
        println!("Error: {}", response.status().as_u16());
    }

    let result: u8 = get_entries(&base_url, &url_store, &project_name);

    if result == 1 {
        println!("Could not get entries");
        std::process::exit(1);
    }
}

fn is_same_domain(base_url: &str, target_url: &str) -> bool {

    if base_url.len() < target_url.len() &&
        target_url.starts_with(base_url) {
        return true;
    }
    if base_url.len() > target_url.len() &&
        base_url.starts_with(target_url) {
        return true;
    }

    false
}

fn create_project_folder(name :&str) -> i8 {
    // Create folder for this project
    match std::fs::create_dir_all(Path::new("project_name")) {
        Ok(_) => println!("Project folder created"),
        Err(error) => {
            println!("Creating project folder, error = {}", error);
            return -1;
        }
    }

    0
}

fn page_is_indexof(url: &str) -> i8 {

    let mut response = match reqwest::get(url) {
        Ok(resp) => resp,
        Err(error) => {
            println!("error: {}", error);
            return -1;
        }
    };

    if !response.status().is_success() {
        println!("Error: {}", response.status().as_u16());
        return -1;
    }

    match response.text() {
        Ok(t) => {
            if !t.contains("<title>Index of") {
                return 0;
            }
        },
        Err(error) => {
            return -1;
        }
    };

    0
}

fn get_entries(url: &str, storage: &Data, project_folder: &str) -> u8 {
    println!("get_entries, url = {}", url);

    let mut response = match reqwest::get(url) {
        Ok(resp) => resp,
        Err(error) => {
            println!("error: {}", error);
            return 1;
        }
    };

    if !response.status().is_success() {
        println!("Error: {}", response.status().as_u16());
        return 1;
    }

    let base_url = url.clone();
    let mut urls: Vec<String> = Vec::new();

    extract_links(&mut response, &mut urls);

    // Remove entry equal to "../"
    urls = urls.into_iter()
        .filter(|href| !href.eq("../"))
        .collect();

    for href in urls {

        // Make sure that href does not start with "/"
        let full_url = format!("{}/{}",
                               base_url.trim_end_matches("/"),
                               href.trim_start_matches("/"));

        if full_url.ends_with("/") {

            let result = page_is_indexof(&full_url);

            if result == -1 {
                println!("Could not check if url is indexof");
                continue;
            }

            if result == 0 {
                // Folder containing more files or just an html file
                // with different html tags
                println!("Getting entries from {}", full_url);
                get_entries(&full_url, storage, &project_folder);
            } else if result == 1 {
                // It is an html page that may be visited later
                storage.add_html_page(full_url);
            }

        } else {
            println!("Downloading: {}", full_url);
            let result = download_entry(&full_url, &project_folder);

            if result == -1 {
                println!("*** Download failed!!! {}", href);
            }
        }
    }

    0
}

fn extract_links(response: &mut Response, store: &mut Vec<String>) -> i8 {
    let result = Document::from_read(response);

    match result {
        Ok(data) => {
            let links = data
                .find(Name("a"))
                .filter_map(|link| link.attr("href"));

            for link in links {
                store.push(link.to_string());
            }
        },
        Err(error) => {
            println!("Could not extract links: {}", error);
            return -1;
        }
    };

    0
}

fn download_entry(entry: &str, project_folder: &str) -> i8 {

    let mut response = match reqwest::get(entry) {
        Ok(resp) => resp,
        Err(error) => {
            println!("*** download_entry, error = {}", error);
            return -1;
        }
    };

    // Split and remove empty elements
    let fields: Vec<&str> = entry.split("/")
        .into_iter()
        .filter(|x| !x.trim().is_empty())
        .collect();

    let num_fields: usize = fields.len();

    // Filename extracted from url. In this case it is the last field
    let output_file: &str = fields[fields.len()-1];

    let url_components_count = fields.len();

    // By default, do not expect subfolders
    let mut output_path = Path::new(&project_folder)
        .join(output_file)
        .to_str().unwrap().to_string();


    if url_components_count > 4 {
        //println!("Number of url components: {}", url_components_count);
        // extract subfolders from url (entry) and
        let subfolders = &fields[3..num_fields-1].join("/").to_string();

        //println!("subfolders: {}", subfolders);

        // create these subfolders if they do not exist
        let result = std::fs::create_dir_all(
            Path::new(&project_folder).join(&subfolders)
        );

        let out_path = match result {
            Ok(_v) => {
                Path::new(&subfolders).join(&output_file)
            },
            Err(error) => {
                println!("Error when setting output path: {}", error);
                return -1;
            }
        };

        output_path = std::env::current_dir().unwrap()
            .join(&project_folder)
            .join(out_path)
            .to_str().unwrap().to_string();
    }

    // Check if full path already exists
    let target_path = Path::new(&output_path);

    if target_path.exists() {
        println!("{}: \n\tPath already exists!!!", output_path);
        return 1;
    }

    //println!("Target path: {}", target_path.to_str().unwrap().to_string());

    // Create and download file
    let mut file = match std::fs::File::create(&output_path) {
        Ok(f) => f,
        Err(error) => {
            println!("\tCreating file, error = {}", error);
            return 1;
        }
    };

    // Save data in file
    println!("Saving file: {}", output_path.as_str());

    let result = match response.copy_to(&mut file) {
        Ok(d) => d,
        Err(error) => {
            println!("Error when saving data in file: {}", error);
            return 1;
        }
    };

    0
}
