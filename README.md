# Indexof downloader

Tool written in Rust languge, for downloading files from "Index of" url (*** WIP ***)
Basic functionality works.

How to build it:

* cargo build --release

How to use it:

* cargo run or get inside "target/release" and type "./indexof_downloader"
